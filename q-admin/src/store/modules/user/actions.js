import axios from 'axios'
import server from 'src/config/server'
import notify from 'src/utils/notify'
import errorHandler from 'src/utils/error-handler'
import { LoadingBar } from 'quasar'

export function init () {

}

export async function registerUser ({ commit, dispatch }, payload) {
  try {
    LoadingBar.start()
    const { email, password, secret, isAdmin } = payload
    return axios.post(`${server.serverURI}/user/create`, {
      email,
      password,
      secret,
      isAdmin
    }).then(response => {
      dispatch('loginUser', { email: response.data.email, password })
      LoadingBar.stop()
    }).catch(error => {
      notify({ message: error?.response?.data.message || error?.response?.data.error || 'Сервер недоступен.' })
    })
  } catch (e) {
    errorHandler(e)
  }
}

export async function loginUser ({ commit }, payload) {
  try {
    LoadingBar.start()
    const { email, password } = payload
    return axios.post(`${server.serverURI}/user/login`, {
      email,
      password
    }).then(response => {
      localStorage.setItem('token', response.data.token)
      commit('mutationUser', response.data.user)
      notify({ message: 'Авторизация прошла успешно.', color: 'positive' })
      LoadingBar.stop()
    }).catch(error => {
      notify({ message: error?.response?.data.message || error?.response?.data.error || 'Сервер недоступен.' })
    })
  } catch (e) {
    errorHandler(e)
  }
}

export async function getUser ({ commit }) {
  try {
    const token = localStorage.getItem('token')
    return axios.get(`${server.serverURI}/user`, {
      headers: {
        Authorization: token
      }
    }).then(response => {
      commit('mutationUser', response.data)
      notify({ message: 'Вход выполнен.', color: 'positive' })
    }).catch(error => {
      notify({ message: error?.response?.data.message || error?.response?.data.error || 'Сервер недоступен.' })
    })
  } catch (e) {
    errorHandler(e)
  }
}

export function logoutUser ({ commit }) {
  localStorage.setItem('token', '')
  commit('mutationUser', null)
}
