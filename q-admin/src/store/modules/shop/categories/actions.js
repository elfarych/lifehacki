import server from 'src/config/server'
import axios from 'axios'
import errorHandler from 'src/utils/error-handler'
import notify from 'src/utils/notify'

export async function loadCategories ({ commit }) {
  try {
    await axios.get(`${server.serverURI}/shop/categories`)
      .then(response => {
        commit('mutationCategories', response.data)
      })
  } catch (e) {
    errorHandler(e)
  }
}

export async function createCategory ({ dispatch }, payload) {
  try {
    await axios.post(`${server.serverURI}/shop/categories/create`, {
      headers: {
        Authorization: localStorage.getItem('token')
      },
      data: {
        ...payload
      }
    })
  } catch (e) {
    errorHandler(e)
  }
}

export async function deleteCategory ({ commit, dispatch }, id) {
  try {
    await axios.delete(`${server.serverURI}/shop/categories/delete/${id}`, {
      headers: {
        Authorization: localStorage.getItem('token')
      }
    })
      .then(response => {
        notify({ message: `Категория "${response.data.name}" удалена.` })
      })
    dispatch('loadCategories')
  } catch (e) {
    errorHandler(e)
  }
}

export function reloadData ({ dispatch }) {
  dispatch('loadCategories')
}
