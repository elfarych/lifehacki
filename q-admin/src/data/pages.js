export default {
  shop: [
    { title: 'Заказы', slug: '/orders', icon: 'las la-clipboard-list' },
    { title: 'Товары', slug: '/products', icon: 'las la-shopping-bag' },
    { title: 'Категории', slug: '/categories', icon: 'las la-layer-group' },
    { title: 'Бренды', slug: '/brands', icon: 'las la-registered' }
  ]
}
