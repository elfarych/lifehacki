const { Strategy, ExtractJwt } = require('passport-jwt')

const { jwtKey } = require('../config/keys')
const User = require('../app/user/models/User')

const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: jwtKey,
}

module.exports = (passport) => {
    passport.use(
        new Strategy(options, async (payload, done) => {
            try {
                const user = await User.findById(payload.userId).select('email isAdmin name id')
                done(null, user || false)
            } catch (e) {
                console.log(e)
            }
        })
    )
}
