const app = require('./app/app')
const mongoose = require('mongoose')

const serverConfig = require('./config/server')
const { mongoConnectionString } = require('./config/keys')

const PORT = process.env.PORT || serverConfig.PORT
const HOST = process.env.HOST || serverConfig.HOST


const start = async () => {
    try {
        app.listen(PORT, HOST, () => console.log(`server started on PORT: ${PORT}, HOST: ${HOST}`))
        await mongoose.connect(mongoConnectionString).then(() => console.log('Mongo connected'))
    } catch (e) {
        console.log(e)
    }
}

start()

