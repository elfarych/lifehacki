const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const User = require('../models/User')
const errorHandler = require('../../../utils/error-heandler')
const { jwtKey, secretKey } = require('../../../config/keys')

/* -------------------------------------------------------------------------- */
/*                                 Create user                                */
/* -------------------------------------------------------------------------- */
module.exports.create = async (req, res) => {
    const { email, password, isAdmin, secret } = req.body
    if (!secret || secret !== secretKey) return res.status(403).json({ message: 'У вас недостаточно полномочий' })

    const salt = bcrypt.genSaltSync(11)
    const user = new User({
        email,
        isAdmin,
        password: bcrypt.hashSync(password, salt),
    })

    try {
        await user.save()
        res.status(201).json(user)
    } catch (e) {
        errorHandler(res, e)
    }
}

/* -------------------------------------------------------------------------- */
/*                                 User login                                 */
/* -------------------------------------------------------------------------- */
module.exports.login = async (req, res) => {
    const { email, password } = req.body

    if (!email || !password) return errorHandler(res, 'email, пароль - обязательные поля')

    const user = await User.findOne({ email })
    if (user && bcrypt.compareSync(password, user.password)) {
        const token = await jwt.sign({ userId: user._id, email: user.email }, jwtKey, {
            expiresIn: '240h',
        })
        res.status(200).json({ token: `Bearer ${token}`, user })
    } else {
        errorHandler(res, 'Неверный логин или пароль')
    }
}
