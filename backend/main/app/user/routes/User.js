const router = require('express').Router()
const controllers = require('../controllers/User')
const passport = require('passport')

// api/user...
router.post('/create', controllers.create)
router.post('/login', controllers.login)
router.get('', passport.authenticate('jwt', { session: false }), (req, res) => {
    res.json(req.user)
})

module.exports = router
