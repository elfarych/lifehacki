class ErrorHandler {
    constructor({ err, res }) {
        this.err = err
        this.res = res
    }

    sendError() {
        this.res
            .status(this.err.status || 400)
            .json({ error: this.err.message || this.err })
    }
}

module.exports = ErrorHandler
