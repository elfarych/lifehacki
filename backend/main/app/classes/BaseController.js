const passport = require('passport')
const ErrorHandler = require('./ErrorHandler')

class BaseController {
    constructor(model) {
        this.model = model
    }

    /* ---------------------------------- Find ---------------------------------- */
    find({ req, res, authRequire = false, callbacks = [] }) {
        const query = req.query
        const find = async () => {
            try {
                const model = await this.model.find({
                    $contains: { name: query.name_contains },
                })
                res.status(200).json(model)

                if (callbacks.length) callbacks.forEach((cb) => cb({ model, req, res }))
            } catch (err) {
                new ErrorHandler({ res, err }).sendError()
            }
        }
        if (authRequire)
            passport.authenticate('jwt', { session: false })(req, res, find)
        else find()
    }

    /* --------------------------------- Find one -------------------------------- */
    async findOne({
        req,
        res,
        updateCounter = false,
        authRequire = false,
        callbacks = [],
    }) {
        const findOne = async () => {
            try {
                const model = await this.model.findById(req.params.id)
                if (updateCounter) {
                    if (model.viewsCount) {
                        model.viewsCount += 1
                        model.save()
                    }
                }
                res.status(200).json(model)

                if (callbacks.length) callbacks.forEach((cb) => cb({ model, req, res }))
            } catch (err) {
                new ErrorHandler({ res, err }).sendError()
            }
        }
        if (authRequire)
            passport.authenticate('jwt', { session: false })(req, res, findOne)
        else findOne()
    }

    /* --------------------------------- Update --------------------------------- */
    async update({ req, res, authRequire = true, callbacks = [] }) {
        const update = async () => {
            try {
                const model = await this.model.updateOne(
                    { _id: req.params.id },
                    { $set: req.body }
                )
                res.status(200).json(model)

                if (callbacks.length) callbacks.forEach((cb) => cb({ model, req, res }))
            } catch (err) {
                new ErrorHandler({ res, err }).sendError()
            }
        }
        if (authRequire)
            passport.authenticate('jwt', { session: false })(req, res, update)
        else update()
    }

    /* --------------------------------- Create --------------------------------- */
    async create({ req, res, authRequire = true, callbacks = [] }) {
        const create = async () => {
            try {
                const model = await new this.model(req.body).save()
                res.status(201).json(model)

                if (callbacks.length) callbacks.forEach((cb) => cb({ model, req, res }))
            } catch (err) {
                new ErrorHandler({ res, err }).sendError()
            }
        }
        if (authRequire)
            passport.authenticate('jwt', { session: false })(req, res, create)
        else create()
    }

    /* --------------------------------- Delete --------------------------------- */
    async delete({ req, res, authRequire = true, callbacks = [] }) {
        const del = async () => {
            try {
                const model = await this.model.findById(req.params.id)
                await model.delete()
                res.status(200).json(model)

                if (callbacks.length) callbacks.forEach((cb) => cb({ model, req, res }))
            } catch (err) {
                new ErrorHandler({ res, err }).sendError()
            }
        }
        if (authRequire)
            passport.authenticate('jwt', { session: false })(req, res, del)
        else del()
    }
}

module.exports = BaseController
