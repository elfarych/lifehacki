const mongoose = require('mongoose')
const { Schema } = mongoose
const slug = require('mongoose-slug-generator')
mongoose.plugin(slug)

const productSchema = new Schema({
    article: String,
    name: {
        type: String,
        required: true,
    },
    slug: {
        type: String,
        slug: 'name',
        unique: true,
    },
    categories: [{ ref: 'Category', type: mongoose.Types.ObjectId }],
    labels: [{ ref: 'Label', type: mongoose.Types.ObjectId }],
    brand: {
        type: mongoose.Types.ObjectId,
        ref: 'Brand',
    },
    image: {
        type: String,
    },
    images: [{ type: String }],
    description: {
        type: String,
    },
    information: {
        type: String,
    },
    price: {
        type: Number,
        required: true,
    },
    oldPrice: {
        type: Number,
    },
    purchasePrice: {
        type: Number,
        required: true,
    },
    quantity: {
        type: Number,
        required: true,
    },
    hot: {
        type: Boolean,
        default: false,
    },
    new: {
        type: Boolean,
        default: false,
    },
    rating: {
        type: Number,
        default: 5,
    },
    viewsCount: {
        type: Number,
        default: 0,
    },
    salesCount: {
        type: Number,
        default: 0,
    },
    created: {
        type: Date,
        default: Date.now,
    },
    updated: {
        type: Date,
    },
})

module.exports = mongoose.model('Product', productSchema)
