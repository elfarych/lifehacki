const mongoose = require('mongoose')
const { Schema } = mongoose
const slug = require('mongoose-slug-generator')

mongoose.plugin(slug)

const categorySchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    slug: {
        type: String,
        slug: 'name',
        unique: true,
    },
    image: {
        type: String,
    },
    productCount: {
        type: Number,
        default: 0,
    },
    viewsCount: {
        type: Number,
        default: 0,
    },
    public: {
        type: Boolean,
        default: true,
    },
    parentCategory: {
        type: mongoose.Types.ObjectId,
        ref: 'Category',
    },
    childCategories: [{ type: mongoose.Types.ObjectId, ref: 'Category' }],
    products: [{ type: mongoose.Types.ObjectId, ref: 'Product' }],
})

module.exports = mongoose.model('Category', categorySchema)
