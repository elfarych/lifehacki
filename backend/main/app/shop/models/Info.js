const mongoose = require('mongoose')
const { Schema } = mongoose

const infoSchema = new Schema({
    categoriesCount: Number,
    productsCount: Number,
    brandsCount: Number,
})

module.exports = mongoose.model('Info', infoSchema)
