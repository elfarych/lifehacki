const mongoose = require('mongoose')
const { Schema } = mongoose
const slug = require('mongoose-slug-generator')
mongoose.plugin(slug)

const brandSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    logo: {
        type: String,
        required: true,
    },
    slug: {
        type: String,
        slug: 'name',
        unique: true,
    },
    information: {
        type: String,
    },
    viewsCount: {
        type: Number,
        default: 0,
    },
    productsCount: {
        type: Number,
        default: 0,
    },
})

module.exports = mongoose.model('Brand', brandSchema)
