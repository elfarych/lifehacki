const mongoose = require('mongoose')
const { Schema } = mongoose
const slug = require('mongoose-slug-generator')
mongoose.plugin(slug)

const labelSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    slug: {
        type: String,
        slug: 'name',
        unique: true,
    },
    information: {
        type: String,
    },
    productsCount: {
        type: Number,
        default: 0,
    },
})

module.exports = mongoose.model('Label', labelSchema)
