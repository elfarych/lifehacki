const router = require('express').Router()
const Product = require('../models/Product')
const BaseController = require('../../classes/BaseController')

const controller = new BaseController(Product)

// Create
router.post('/create', (req, res) => controller.create({ req, res, callbacks: [updateCategoryProductsCount] }))

// Update
router.put('/update/:id', (req, res) => controller.update({ req, res, callbacks: [updateCategoryProductsCount] }))

// Delete
router.delete('/delete/:id', (req, res) => controller.delete({ req, res, callbacks: [updateCategoryProductsCount] }))

// Find
router.get('', (req, res) => controller.find({ req, res }))

// Find one
router.get('/:id', (req, res) => controller.findOne({ req, res, callbacks: [updateProductViewsCount] }))

function updateCategoryProductsCount() {
    // !TODO Доработать обновление счетчика продуктов категории
}

function updateProductViewsCount({ model, req, res }) {
    // !TODO Доработать обновление счетчика просмотров продукта
    model.viewsCount += 1
    model.save()
}

module.exports = router
