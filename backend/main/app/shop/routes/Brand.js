const router = require('express').Router()
const Brand = require('../models/Brand')
const BaseController = require('../../classes/BaseController')

const controller = new BaseController(Brand)

// Create
router.post('/create', (req, res) => controller.create({ req, res }))

// Delete
router.delete('/delete/:id', (req, res) => controller.delete({ req, res }))

// Update
router.put('/update/:id', (req, res) => controller.update({ req, res }))

// Find
router.get('', (req, res) => controller.find({ req, res }))

// Find one
router.get('/:id', (req, res) => controller.findOne({ req, res, updateCounter: true }))

module.exports = router
