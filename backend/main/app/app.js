const express = require('express')
const app = express()
const passport = require('passport')
const cors = require('cors')
const multer = require('multer')
const mainRouter = require('./router')

const userRoutes = require('./user/routes/User')
/* -------------------------------------------------------------------------- */
/*                             Passport                            */
/* -------------------------------------------------------------------------- */
app.use(passport.initialize())
require('../middleware/passport')(passport)

/* -------------------------------------------------------------------------- */
/*                                    Utils                                   */
/* -------------------------------------------------------------------------- */
app.use(require('morgan')('dev'))
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(cors())
app.use(multer({ dest: 'uploads/images' }).single('file'))
app.use(express.static('uploads'))
/* -------------------------------------------------------------------------- */
/*                                   Routes                                   */
/* -------------------------------------------------------------------------- */
mainRouter(app)

module.exports = app
