// Shop routes
const categoryRoutes = require('./shop/routes/Category')
const productRoutes = require('./shop/routes/Product')
const brandRoutes = require('./shop/routes/Brand')
const labelRoutes = require('./shop/routes/Label')

// Main routes
const sliderRoutes = require('./main/routes/Slider')

// User routes
const userRoutes = require('./user/routes/User')

const { API_PREFIX } = require('../config/server')

module.exports = (app) => {
    app.use(API_PREFIX + '/shop/categories', categoryRoutes)
    app.use(API_PREFIX + '/shop/products', productRoutes)
    app.use(API_PREFIX + '/shop/brands', brandRoutes)
    app.use(API_PREFIX + '/shop/labels', labelRoutes)

    app.use(API_PREFIX + '/main/slider', sliderRoutes)
    app.use(API_PREFIX + '/user', userRoutes)
}
