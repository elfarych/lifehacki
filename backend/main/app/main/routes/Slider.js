const router = require('express').Router()
const Slider = require('../models/Slider')
const BaseContoller = require('../../classes/BaseController')

const controller = new BaseContoller(Slider)

router.get('', (req, res) => controller.find({ req, res }))

module.exports = router
