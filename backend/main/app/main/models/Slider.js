const mongoose = require('mongoose')
const { Schema } = mongoose

const sliderSchema = new Schema({
    title: {
        type: String,
    },
    image: {
        type: String,
    },
    url: {
        type: String,
    },
    order: {
        type: Number,
    },
    viewsCount: {
        type: Number,
        default: 0,
    },
    created: {
        type: Date,
        default: Date.now,
    },
})

module.exports = mongoose.model('slider', sliderSchema)
